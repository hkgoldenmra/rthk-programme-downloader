@echo off

rem This bat file requires curl, grep, sed, ffmpeg commands to run.
rem Make sure all exe and dll files are located in the library directory.
rem You can edit channel, date, time, output_file parameters below to download the audio stream via ffmpeg from RTHK.
rem channel must select 1, 2, 3, 4, 5, 6, where 6 is PuTongHua channel.
rem date must enter a YYYYMMDD date format.
rem time must enter a HHMM time format.
rem output_file enter any windows available characters.
rem Then execute this bat file to download the target.

rem input option
set channel=1
set date=20190623
set time=1000

rem output option
set audio_codec=libmp3lame
set audio_bitrate=128k
set audio_hz=44100
set audio_channel=2
set output_file=output

rem !!!!! Don't edit information below if you don't know how to edit !!!!!

if "%channel%"=="6" (
	set channel=pth
) else if "%channel%"=="5" (
	set channel=radio5
) else if "%channel%"=="4" (
	set channel=radio4
) else if "%channel%"=="3" (
	set channel=radio3
) else if "%channel%"=="2" (
	set channel=radio2
) else (
	set channel=radio1
)

set temp_file=temp.txt
set output_dir=output-dir
set output_ext=mp3

set LIBRARY_DIR=library
set CURL_FILE=%LIBRARY_DIR%\curl
set GREP_FILE=%LIBRARY_DIR%\grep
set SED_FILE=%LIBRARY_DIR%\sed
set FFMPEG_FILE=%LIBRARY_DIR%\ffmpeg

set REQUEST="GET"
set CONTENT_TYPE=application/x-www-form-urlencoded
set CHARSET=UTF-8
set HEADER_CONTENT_TYPE="Content-Type: %CONTENT_TYPE%; charset=%CHARSET%"
set PROTOCOL=https
set DOMAIN=programme.rthk.hk
set MYPATH=/channel/radio/player_txt.php
set URL="%PROTOCOL%://%DOMAIN%%MYPATH%?mychannel=%channel%&mydate=%date%&mytime=%time%"
set GREP_PARAMETER="m3u8'"
set SED_PARAMETER="s/^.+(http.+m3u8).+$/\1/g"

"%CURL_FILE%" -sL --request %REQUEST% --header %HEADER_CONTENT_TYPE% %URL% | "%GREP_FILE%" %GREP_PARAMETER% | "%SED_FILE%" -r %SED_PARAMETER% >"%temp_file%"
set /p url=<"%temp_file%"
erase "%temp_file%"
if not exist "%output_dir%" (
	mkdir "%output_dir%"
)
"%FFMPEG_FILE%" -y -i "%url%" -vn -acodec "%audio_codec%" -ab "%audio_bitrate%" -ar "%audio_hz%" -ac "%audio_channel%" "%output_dir%\%output_file%-%channel%-%date%-%time%.%output_ext%"
