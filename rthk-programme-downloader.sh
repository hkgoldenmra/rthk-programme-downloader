#!/bin/bash

# This sh file requires curl, grep, sed, ffmpeg commands to run.
# ffmpeg - apt-get install ffmpeg
# curl32 - apt-get install curl
# grep - apt-get install grep
# sed - apt-get install sed
# Make sure all exe and dll files are located in the library directory.
# You can edit channel, date, time, output_file parameters below to download the audio stream via ffmpeg from RTHK.
# channel must select 1, 2, 3, 4, 5, 6, where 6 is PuTongHua channel.
# date must enter a YYYYMMDD date format.
# time must enter a HHMM time format.
# output_file enter any windows available characters.
# Then execute this bat file to download the target.

# input option
channel="1"
date="20190623"
time="1000"

# output option
audio_codec="libmp3lame"
audio_bitrate="128k"
audio_hz="44100"
audio_channel="2"
output_file="output"

# !!!!! Don't edit information below if you don't know how to edit !!!!!

if [ "${channel}" = "6" ]; then
	channel="pth"
elif [ "${channel}" = "5" ]; then
	channel="radio5"
elif [ "${channel}" = "4" ]; then
	channel="radio4"
elif [ "${channel}" = "3" ]; then
	channel="radio3"
elif [ "${channel}" = "2" ]; then
	channel="radio2"
else
	channel="radio1"
fi

temp_file="temp.txt"
output_dir="output-dir"
output_ext="mp3"

# LIBRARY_DIR=library
CURL_FILE="curl"
GREP_FILE="grep"
SED_FILE="sed"
FFMPEG_FILE="ffmpeg"

REQUEST="GET"
CONTENT_TYPE="application/x-www-form-urlencoded"
CHARSET="UTF-8"
HEADER_CONTENT_TYPE="Content-Type: ${CONTENT_TYPE}; charset=${CHARSET}"
PROTOCOL="https"
DOMAIN="programme.rthk.hk"
MYPATH="/channel/radio/player_txt.php"
URL="${PROTOCOL}://${DOMAIN}${MYPATH}?mychannel=${channel}&mydate=${date}&mytime=${time}"
GREP_PARAMETER="m3u8'"
SED_PARAMETER="s/^.+(http.+m3u8).+\$/\1/g"

command="'${CURL_FILE}' -sL --request '${REQUEST}' --header '${HEADER_CONTENT_TYPE}' '${URL}' | '${GREP_FILE}' \"${GREP_PARAMETER}\" | '${SED_FILE}' -r '${SED_PARAMETER}'"
echo "${command}"
url=`eval "${command}"`
if ! [ -d "${output_dir}" ]; then
	mkdir "${output_dir}"
fi
"${FFMPEG_FILE}" -y -i "${url}" -vn -acodec "${audio_codec}" -ab "${audio_bitrate}" -ar "${audio_hz}" -ac "${audio_channel}" "${output_dir}/${output_file}-${channel}-${date}-${time}.${output_ext}"
