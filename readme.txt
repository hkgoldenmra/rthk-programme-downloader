# Abstract
Hong Kong's visually impaired people like to collect and listen to RTHK's broadcast programmes.

However, because the programmes are played in stream method, they are not audio files.

Therefore, downloading these streams cannot be directly saved as new files.

Downloading requires the ability to combine streaming information.
# Introduction
This project requires ffmpeg, curl, grep, sed commands to run.
## Packages and Files
Require these packages

* ffmpeg
* curl
* grep
* sed
### Linux
Use root or super user to install these packages
```
apt-get install ffmpeg curl grep sed
```
### Windows
Download resources

* ffmpeg 32 bit
	* [https://ffmpeg.zeranoe.com/builds/win32/static/ffmpeg-latest-win32-static.zip](https://ffmpeg.zeranoe.com/builds/win32/static/ffmpeg-latest-win32-static.zip "https://ffmpeg.zeranoe.com/builds/win32/static/ffmpeg-latest-win32-static.zip")
* curl 32 bit
	* [https://curl.haxx.se/windows/dl-7.64.1_1/curl-7.64.1_1-win32-mingw.zip](https://curl.haxx.se/windows/dl-7.64.1_1/curl-7.64.1_1-win32-mingw.zip "https://curl.haxx.se/windows/dl-7.64.1_1/curl-7.64.1_1-win32-mingw.zip")
* grep dependency
	* [https://nchc.dl.sourceforge.net/project/gnuwin32/grep/2.5.4/grep-2.5.4-dep.zip](https://nchc.dl.sourceforge.net/project/gnuwin32/grep/2.5.4/grep-2.5.4-dep.zip "https://nchc.dl.sourceforge.net/project/gnuwin32/grep/2.5.4/grep-2.5.4-dep.zip")
* grep
	* [https://nchc.dl.sourceforge.net/project/gnuwin32/grep/2.5.4/grep-2.5.4-bin.zip](https://nchc.dl.sourceforge.net/project/gnuwin32/grep/2.5.4/grep-2.5.4-bin.zip "https://nchc.dl.sourceforge.net/project/gnuwin32/grep/2.5.4/grep-2.5.4-bin.zip")
* sed dependency
	* [https://nchc.dl.sourceforge.net/project/gnuwin32/sed/4.2.1/sed-4.2.1-dep.zip](https://nchc.dl.sourceforge.net/project/gnuwin32/sed/4.2.1/sed-4.2.1-dep.zip "https://nchc.dl.sourceforge.net/project/gnuwin32/sed/4.2.1/sed-4.2.1-dep.zip")
* sed
	* [https://nchc.dl.sourceforge.net/project/gnuwin32/sed/4.2.1/sed-4.2.1-bin.zip](https://nchc.dl.sourceforge.net/project/gnuwin32/sed/4.2.1/sed-4.2.1-bin.zip "https://nchc.dl.sourceforge.net/project/gnuwin32/sed/4.2.1/sed-4.2.1-bin.zip")
#### Structure
Make sure all exe and dependency files are located in the library directory, such as

* .\\library\\
* .\\library\\<ffmpeg 32 bit>
* .\\library\\<ffplay 32 bit>
* .\\library\\<ffserver 32 bit>
* .\\library\\<curl 32 bit>
* .\\library\\<curl-dependency>
* .\\library\\<curl-cert>
* .\\library\\<grep>
* .\\library\\<grep-dependency>
* .\\library\\<sed>
* .\\library\\<sed-dependency>
* .\\batch-file.bat
## How to run
You can edit the parameters below in batch file to download the stream via ffmpeg from RTHK.

* channel
	* the RTHK channel, 1, 2, 3, 4, 5 are available.
* date
	* the RTHK programme date, YYYYMMDD date format.
* time
	* the RTHK programme time, HHMM time format.
* audio_codec
	* the output audio codec, preset libmp3lame.
* audio_bitrate
	* the output audio biterate, preset 128kbps.
* audio_hz
	* the output audio hz, preset 44100.
* audio_channel
	* the output audio channels, preset 2.
* output_file
	* the output file name, preset output.

Then execute the bat files or shell files to download the target.